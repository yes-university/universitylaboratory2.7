﻿using yesUniversity;

namespace UniversityLaboratory2._7
{
    class Program
    {
        private static void Main()
        {
            ConsoleMessageGenerator.GenerateLaboratoryTitle(
                new LaboratoryInfo(
                    "Бродский Егор Станиславович",
                    "ИР-133Б",
                    7,
                    "КЛАСИ ТА МЕТОДИ"
                )
            );
            Console.WriteLine();
            TaskChooser.Run(new TaskInfo[]
            {
                new("Завдання 1", () =>
                {
                    var matrixB = Utils.GenerateRandomArray(4, -10, +10, 4);
                    var matrixD = Utils.GenerateRandomArray(3, -10, +10, 3);
                    var transposedMatrixB = Utils.Transpose(matrixB);
                    var transposedMatrixD = Utils.Transpose(matrixD);
                    Console.WriteLine("Матриця B:");
                    Utils.PrintMatrix(matrixB);
                    Console.WriteLine("Матриця D:");
                    Utils.PrintMatrix(matrixD);
                    Console.WriteLine("Матриця B^T:");
                    Utils.PrintMatrix(transposedMatrixB);
                    Console.WriteLine("Матриця D^T:");
                    Utils.PrintMatrix(transposedMatrixD);
                    Console.WriteLine("Матриця B^T * B:");
                    Utils.PrintMatrix(Utils.MultiplyMatrix(transposedMatrixB, matrixB));
                    Console.WriteLine("Матриця D^T * D:");
                    Utils.PrintMatrix(Utils.MultiplyMatrix(transposedMatrixD, matrixD));
                    return null;
                }),
            });
        }
    }
}