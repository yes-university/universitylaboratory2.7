﻿namespace yesUniversity;

public class ConsoleMessageGenerator
{
    public static void GenerateMessageFrom(string[] args, bool makeSpacing = false)
    {
        foreach (var s in args)
        {
            Console.WriteLine(s);
            if (makeSpacing)
            {
                Console.WriteLine();
            }
        }
    }

    public static void GenerateLaboratoryTitle(LaboratoryInfo laboratoryInfo)
    {
        GenerateMessageFrom(new string[]
        {
            "Лабораторная работа №" + laboratoryInfo.laboratoryId, 
            "",
            "Выполнил: "+laboratoryInfo.laboratoryOwner,
            "Группа: "+laboratoryInfo.laboratoryOwnerGroup,
            "Наименование ЛР: "+laboratoryInfo.laboratoryTitle,
        });
    }
}

public class LaboratoryInfo
{
    public readonly string laboratoryOwner;
    public readonly string laboratoryOwnerGroup;
    public readonly int laboratoryId;
    public readonly string laboratoryTitle;

    public LaboratoryInfo(string laboratoryOwner, string laboratoryOwnerGroup, int laboratoryId, string laboratoryTitle)
    {
        this.laboratoryOwner = laboratoryOwner;
        this.laboratoryOwnerGroup = laboratoryOwnerGroup;
        this.laboratoryId = laboratoryId;
        this.laboratoryTitle = laboratoryTitle;
    }
}