﻿namespace yesUniversity;

public static class TaskChooser
{
    public static void Run(TaskInfo[] taskInfos)

    {
        bool isUse = true;
        while (isUse)
        {
            Console.WriteLine("======");
            for (int i = 0; i < taskInfos.Length; i++)
            {
                string name = taskInfos[i].name;
                Console.WriteLine($"{i + 1}) {name}");
            }

            Console.WriteLine("Введите номер задачи или нажмите Enter для завершения");
            int nextTask = IntConsoleEnter();
            if (nextTask == 0)
            {
                isUse = false;
                Console.WriteLine("Bye!");
            }
            else
            {
                try
                {
                    taskInfos[nextTask - 1].execute();
                    Console.WriteLine();
                }
                catch (Exception e)
                {
                    Console.WriteLine();
                    Console.WriteLine("=== Err ===");
                    Console.WriteLine(e);
                }
            }
        }
    }

    private static int IntConsoleEnter()

    {
        return Utils.ToInt(Console.ReadLine());
    }
}

public class TaskInfo
{
    public readonly string name;
    public readonly Func<int?> execute;

    public TaskInfo(string name, Func<int?> execute)
    {
        this.name = name;
        this.execute = execute;
    }
}
